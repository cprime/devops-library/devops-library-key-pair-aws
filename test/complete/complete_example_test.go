package test

import (
	"strings"
	"testing"

	"github.com/gruntwork-io/terratest/modules/terraform"
	"github.com/stretchr/testify/assert"
)

// Test the complete VPC Example in https://gitlab.com/cprime/devops-library/devops-library-terraform-module-project-template-aws
func TestTerraformCompleteExample(t *testing.T) {
	t.Parallel()

	terraformOptions := &terraform.Options{
		// The path to where our Terraform code is located
		TerraformDir: "../../examples/complete",
		Upgrade:      true,
		// Variables to pass to our Terraform code using -var-file options
		VarFiles: []string{"complete.tfvars"},
	}

	// At the end of the test, run `terraform destroy` to clean up any resources that were created
	defer terraform.Destroy(t, terraformOptions)

	// This will run `terraform init` and `terraform apply` and fail the test if there are any errors
	terraform.InitAndApply(t, terraformOptions)

	// Run `terraform output` to get the value of an output variable
	keyName := terraform.Output(t, terraformOptions, "key_name")

	// Flossed key_name
	keyName = strings.Trim(keyName, "\"")

	expectedKeyName := "eg-test-aws-key-pair"
	// Verify we're getting back the outputs we expect
	assert.Equal(t, expectedKeyName, keyName)

}
